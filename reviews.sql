-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2021 at 01:59 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `searchchallenge`
--

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED DEFAULT NULL,
  `reviewId` bigint(20) UNSIGNED DEFAULT NULL,
  `reviewFullText` varchar(255) DEFAULT NULL,
  `reviewText` varchar(255) DEFAULT NULL,
  `numLikes` int(10) UNSIGNED DEFAULT NULL,
  `numComments` int(10) UNSIGNED DEFAULT NULL,
  `numShares` int(10) UNSIGNED DEFAULT NULL,
  `rating` int(10) UNSIGNED DEFAULT NULL,
  `reviewCreatedOn` varchar(255) DEFAULT NULL,
  `reviewCreatedOnDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `reviewCreatedOnTime` bigint(20) DEFAULT NULL,
  `reviewerId` int(11) DEFAULT NULL,
  `reviewerUrl` varchar(255) DEFAULT NULL,
  `reviewerName` varchar(255) DEFAULT NULL,
  `reviewerEmail` varchar(32) DEFAULT NULL,
  `sourceType` varchar(32) DEFAULT NULL,
  `isVerified` tinyint(1) DEFAULT NULL,
  `source` varchar(32) DEFAULT NULL,
  `sourceName` varchar(32) DEFAULT NULL,
  `sourceId` varchar(255) DEFAULT NULL,
  `href` varchar(255) DEFAULT NULL,
  `logoHref` varchar(255) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `reviewId`, `reviewFullText`, `reviewText`, `numLikes`, `numComments`, `numShares`, `rating`, `reviewCreatedOn`, `reviewCreatedOnDate`, `reviewCreatedOnTime`, `reviewerId`, `reviewerUrl`, `reviewerName`, `reviewerEmail`, `sourceType`, `isVerified`, `source`, `sourceName`, `sourceId`, `href`, `logoHref`, `reg_date`) VALUES
(2097047, 93, '5 star review', '5 star review', 0, 0, 0, 5, '2 months ago', '2021-01-25 12:00:35', 1611579635, NULL, NULL, 'Reviewer #20', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:11'),
(2097046, 60000000, '4 star review', '4 star review', 0, 0, 0, 4, '2 months ago', '2021-01-25 12:00:21', 1611579621, NULL, NULL, 'Reviewer #19', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:11'),
(2097045, 0, '3 star review', '3 star review', 0, 0, 0, 3, '2 months ago', '2021-01-25 12:00:10', 1611579610, NULL, NULL, 'Reviewer #18', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097044, 0, '2 star review', '2 star review', 0, 0, 0, 2, '2 months ago', '2021-01-25 11:59:57', 1611579597, NULL, NULL, 'Reviewer #17', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097043, 10588, '1 star review', '1 star review', 0, 0, 0, 1, '2 months ago', '2021-01-25 11:59:40', 1611579580, NULL, NULL, 'Reviewer #16', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097042, 1, '5 star review', '5 star review', 0, 0, 0, 5, '2 months ago', '2021-01-25 11:59:27', 1611579567, NULL, NULL, 'Reviewer #15', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097041, 0, '', '', 0, 0, 0, 4, '2 months ago', '2021-01-25 11:59:15', 1611579555, NULL, NULL, 'Reviewer #14', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097040, 65, '3 star review', '3 star review', 0, 0, 0, 3, '2 months ago', '2021-01-25 11:58:46', 1611579526, NULL, NULL, 'Reviewer #13', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097039, 0, '', '', 0, 0, 0, 2, '2 months ago', '2021-01-25 11:58:22', 1611579502, NULL, NULL, 'Reviewer #12', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097038, 0, '1 star review', '1 star review', 0, 0, 0, 1, '2 months ago', '2021-01-25 11:58:06', 1611579486, NULL, NULL, 'Reviewer #11', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097037, 70, '', '', 0, 0, 0, 5, '2 months ago', '2021-01-25 11:57:48', 1611579468, NULL, NULL, 'Reviewer #10', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097036, 59, '4 star review', '4 star review', 0, 0, 0, 4, '2 months ago', '2021-01-25 11:57:35', 1611579455, NULL, NULL, 'Reviewer #9', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097034, 0, '', '', 0, 0, 0, 3, '2 months ago', '2021-01-25 11:57:11', 1611579431, NULL, NULL, 'Reviewer #8', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097032, 0, '2 star review', '2 star review', 0, 0, 0, 2, '2 months ago', '2021-01-25 11:56:55', 1611579415, NULL, NULL, 'Reviewer #7', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097031, 0, '', '', 0, 0, 0, 1, '2 months ago', '2021-01-25 11:56:34', 1611579394, NULL, NULL, 'Reviewer #6', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097030, 0, '5 star review', '5 star review', 0, 0, 0, 5, '2 months ago', '2021-01-25 11:56:17', 1611579377, NULL, NULL, 'Reviewer #5', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097029, 3, '4 star review', '4 star review', 0, 0, 0, 4, '2 months ago', '2021-01-25 11:55:57', 1611579357, NULL, NULL, 'Reviewer #4', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097028, 1223, '3 star review', '3 star review', 0, 0, 0, 3, '2 months ago', '2021-01-25 11:55:40', 1611579340, NULL, NULL, 'Reviewer #3', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097027, 5, '2 star review', '2 star review', 0, 0, 0, 2, '2 months ago', '2021-01-25 11:55:21', 1611579321, NULL, NULL, 'Reviewer #2', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12'),
(2097026, 691, '1 star review', '1 star review', 0, 0, 0, 1, '2 months ago', '2021-01-25 11:55:06', 1611579306, NULL, NULL, 'Reviewer #1', NULL, 'custom', 0, 'custom', '1-20 Reviews', '890cdd7974cdf8aabe6e9051f5a87303bdb933ae', NULL, NULL, '2021-04-11 11:52:12');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
