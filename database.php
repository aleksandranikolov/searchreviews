<?php

$servername = "localhost";
$username = "root";
$password = "";

try {
  $conn = new PDO("mysql:host=$servername;dbname=searchchallenge", $username, $password);

  echo "Connected successfully";
} catch (PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}

$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = "CREATE TABLE reviews (
  id BIGINT UNSIGNED,
  reviewId BIGINT UNSIGNED ,
  reviewFullText VARCHAR(255),
  reviewText VARCHAR(255),
  numLikes INT UNSIGNED,
  numComments INT UNSIGNED,
  numShares INT UNSIGNED,
  rating INT UNSIGNED,
  reviewCreatedOn VARCHAR(255),
  reviewCreatedOnDate TIMESTAMP,
  reviewCreatedOnTime BIGINT,
  reviewerId INT,
  reviewerUrl VARCHAR(255),
  reviewerName VARCHAR(255),
  reviewerEmail VARCHAR(32),
  sourceType VARCHAR(32),
  isVerified BOOLEAN,
  source VARCHAR(32),
  sourceName VARCHAR(32),
  sourceId VARCHAR(255),
  href VARCHAR(255),
  logoHref VARCHAR(255),
  reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  )";
$conn->exec($sql);


// Convert JSON
$Json = file_get_contents('reviews.json');
$data = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Json));

// Insert into Database
foreach ($data as $inv) {
  $stmt = $conn->prepare('insert into reviews(id, reviewId, reviewFullText, reviewText, numLikes, 
  numComments, numShares, rating, reviewCreatedOn, reviewCreatedOnDate, reviewCreatedOnTime, reviewerId, reviewerUrl, 
  reviewerName, reviewerEmail, sourceType, isVerified, source, sourceName, sourceId, href, logoHref) values(:id, :reviewId, :reviewFullText, :reviewText, :numLikes, 
  :numComments, :numShares, :rating, :reviewCreatedOn, :reviewCreatedOnDate, :reviewCreatedOnTime, :reviewerId, 
  :reviewerUrl, :reviewerName, :reviewerEmail, :sourceType,
  :isVerified, :source, :sourceName, :sourceId, :href, :logoHref)');
  $stmt->bindValue('id', $inv->id);
  $stmt->bindValue('reviewId', $inv->reviewId);
  $stmt->bindValue('reviewFullText', $inv->reviewFullText);
  $stmt->bindValue('reviewText', $inv->reviewText);
  $stmt->bindValue('numLikes', $inv->numLikes);
  $stmt->bindValue('numComments', $inv->numComments);
  $stmt->bindValue('numShares', $inv->numShares);
  $stmt->bindValue('rating', $inv->rating);
  $stmt->bindValue('reviewCreatedOn', $inv->reviewCreatedOn);
  $stmt->bindValue('reviewCreatedOnDate', $inv->reviewCreatedOnDate);
  $stmt->bindValue('reviewCreatedOnTime', $inv->reviewCreatedOnTime);
  $stmt->bindValue('reviewerId', $inv->reviewerId);
  $stmt->bindValue('reviewerUrl', $inv->reviewerUrl);
  $stmt->bindValue('reviewerName', $inv->reviewerName);
  $stmt->bindValue('reviewerEmail', $inv->reviewerEmail);
  $stmt->bindValue('sourceType', $inv->sourceType);
  $stmt->bindValue('isVerified', $inv->isVerified);
  $stmt->bindValue('source', $inv->source);
  $stmt->bindValue('sourceName', $inv->sourceName);
  $stmt->bindValue('sourceId', $inv->sourceId);
  $stmt->bindValue('href', $inv->href);
  $stmt->bindValue('logoHref', $inv->logoHref);

  $stmt->execute();
}
?>

