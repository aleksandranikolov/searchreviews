<?php

$connect = new PDO("mysql:host=localhost;dbname=searchchallenge", "root", "");
$query = "SELECT * FROM reviews";

$query .= " WHERE rating >= " . strval($_POST['ratingFilter']);
$query .= " ORDER BY";
if ($_POST["text"] == "yes") {
    $query .= " length( reviewFullText) DESC, ";
}
if ($_POST["ratingSorting"] == "highest") {
    $query .= " rating DESC";
} else {
    $query .= " rating ASC";
}

if ($_POST["date"] == "oldest") {
    $query .= " , reviewCreatedOnDate ASC";
} else {
    $query .= " , reviewCreatedOnDate DESC";
}



$statement = $connect->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$total_row = $statement->rowCount();

$output = '';
$start = '';

if ($total_row > 0) {
    foreach ($result as $row) {
        $output .= '
  <tr>
   <td>' . $row["reviewId"] . '</td>
   <td>' . $row["reviewFullText"] . '</td>
   <td>' . $row["numLikes"] . '</td>
   <td>' . $row["numComments"] . '</td>
   <td>' . $row["numShares"] . '</td>
   <td>' . $row["rating"] . '</td>
   <td>' . $row["reviewerName"] . '</td>
   <td>' . $row["reviewCreatedOnDate"] . '</td>
  </tr>
  ';
    }
} else {
    $output .= '
 <tr>
  <td colspan="5" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;




