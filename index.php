<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Review</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />


    <link href="css/bootstrap-select.min.css" rel="stylesheet" />
    <script src="js/bootstrap-select.min.js"></script>
    <title>Reviews</title>
</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 sm-12 offset-3">
                <h2 class="pt-5">Filter reviews</h2>
                <form action="" method="POST" id="reviews">

                    <div class="form-group pt-3">
                        <label class="font-weight-bold" for="orderByRating">Order by rating:</label>
                        <select class="form-control" name="rating" id='searchByRating'>
                            <option value="highest">Highest First</option>
                            <option value="lowest">Lowest First</option>
                        </select>
                    </div>

                    <div class="form-group pt-3">
                        <label class="font-weight-bold" for="rating">Minimum Rating:</label>
                        <select class="form-control" id="rating_number" name="rating_number">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>

                    <div class="form-group pt-3">
                        <label class="font-weight-bold" for="date">Order By Date:</label>
                        <select class="form-control" id="date" name="date">
                            <option value="oldest">Oldest First</option>
                            <option value="newest">Newest First</option>
                        </select>
                    </div>

                    <div class="form-group pt-3">
                        <label class="font-weight-bold" for="text">Prioritize by text:</label>
                        <select class="form-control" id="text" name="text">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>

                    <button type="button" name="search" id="submit" class="btn btn-primary  mt-4 mb-5">Filter</button>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 sm-12 offset-2">
                <table class="table table-striped table-borderd">
                    <thead>
                        <tr>
                            <th>Review ID</th>
                            <th>Review Full Text</th>
                            <th>Review Likes</th>
                            <th>Number Of Comments</th>
                            <th>Number Of Shares</th>
                            <th>Rating</th>
                            <th>Reviewer Name</th>
                            <th>Review Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {
            load_data();

            function load_data(event) {
                if(event) {
                    event.preventDefault();
                }
                $.ajax({
                    url: "fetch.php",
                    method: "POST",
                    data: {
                        ratingSorting: $("#searchByRating").val(),
                        ratingFilter: $("#rating_number").val(),
                        date: $("#date").val(),
                        text: $("#text").val()
                    },
                    success: function(data) {
                        $('tbody').html(data);
                    }
                })
            }

            $("#submit").click(load_data);
        });
    </script>
    
</body>
</html>